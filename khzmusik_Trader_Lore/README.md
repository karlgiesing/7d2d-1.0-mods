# Trader Lore

Adds lore and history to the trader dialogs.

This lore is definitely _not_ the official lore of The Fun Pimps.
It is, however, based in Arizona reality.

* :heavy_check_mark: Server side only (clients do not need to install the mod)
* :heavy_check_mark: EAC safe (clients and servers do not need to disable Easy Anti-Cheat)

## Changes and Features

* Fleshed-out lore and backstory for the Duke, Noah, Whiteriver, and the Cassadores
* Different traders have different relationships with each group or leader,
  and have different opinions or amounts of knowledge about them
* The vanilla dialog window is larger, to make room for more text

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.

Because this modlet only affects trader dialog options,
starting a new game should not be necessary.

## Possible improvements

The lore in this modlet contains a lot of words that are specific to America and Arizona.
It is almost certainly impossible to correctly translate using automated translation services.

If there are non-English speakers who can tranlate the text by hand, please contact me.
I need all the help I can get!
