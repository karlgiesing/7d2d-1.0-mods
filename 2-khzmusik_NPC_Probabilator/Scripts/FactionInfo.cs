﻿using System.Collections.Generic;
using System.Linq;

namespace NPCProbabilator.Scripts
{
    public class FactionInfo
    {
        public string Name { get; set; }

        public bool IsEnemy { get; set; }

        public bool IsHuman { get; set; }

        public List<CharacterInfo> AllCharacters { get; set; }

        public List<CharacterInfo> MeleeCharacters =>
            AllCharacters?.Where(cInfo => cInfo.Weapon?.IsRanged == false).ToList();

        public List<CharacterInfo> RangedCharacters =>
            AllCharacters?.Where(cInfo => cInfo.Weapon?.IsRanged == true).ToList();

        public List<EntityGroupInfo> All { get; set; }
        public List<EntityGroupInfo> Ranged { get; set; }
        public List<EntityGroupInfo> Melee { get; set; }
    }
}
