﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using static DroneWeapons;

namespace NPCProbabilator.Scripts
{
    public class ProbabilityCalculator
    {
        private const string BASELINE_CHARACTER = "__BASELINE__";

        private const string XML_DTD = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

        // Total probability in the entity group
        private const float PROB_TOTAL = 100f;

        // Total probability for all characters in a biome - the rest will be "none"
        private const float BIOME_PROB = 3f;

        private static bool Debug => GamePrefs.GetBool(EnumGamePrefs.DebugMenuEnabled);

        // Tags that indicate the entity is an NPC
        private static readonly FastTags<TagGroup.Global> NpcTags =
            FastTags<TagGroup.Global>.Parse("npc");

        // Damage resistance tags - NPCs with these tags get additional damage resistance
        private static readonly FastTags<TagGroup.Global> DRLow =
            FastTags<TagGroup.Global>.Parse("DRLow");
        private static readonly FastTags<TagGroup.Global> DRMid =
            FastTags<TagGroup.Global>.Parse("DRMid");
        private static readonly FastTags<TagGroup.Global> DRHigh =
            FastTags<TagGroup.Global>.Parse("DRHigh");

        // Biome tags
        private static readonly FastTags<TagGroup.Global> AllBiomeTags =
            FastTags<TagGroup.Global>.Parse("pine_forest,burnt_forest,desert,snow,wasteland");
        private static readonly FastTags<TagGroup.Global> NoBiomeTags =
            FastTags<TagGroup.Global>.Parse("nobiome,nobiomes");
        
        // Day/Night tags
        private static readonly FastTags<TagGroup.Global> DayTags =
            FastTags<TagGroup.Global>.Parse("day,daytime");
        private static readonly FastTags<TagGroup.Global> NightTags =
            FastTags<TagGroup.Global>.Parse("night,nighttime");

        // Tags that indicate the entity is a boss, and only they should spawn into "Boss" volumes
        private static readonly FastTags<TagGroup.Global> BossTags =
            FastTags<TagGroup.Global>.Parse("boss");

        // Tags that indicate the entity should not spawn into the NPC prefab (POI) groups
        private static readonly FastTags<TagGroup.Global> NoPrefabTags =
            FastTags<TagGroup.Global>.Parse("noprefab,nopoi");

        private static readonly int[] Gamestages = new[] { 1, 50, 100, 200, 400, 800 };

        private static readonly string[] HumanFactions = new[] { "bandits", "military", "vault", "whisperers", "whiteriver", "mechs" };

        private static float WeaponAvgNormalized { get; set; }

        public static void DebugLog(string msg)
        {
            if (Debug)
            {
                Log.Out($"Generate NPC Probabilities: {msg}");
            }
        }

        /// <summary>
        /// Calculates the probabilities for NPCs and writes them to XML files.
        /// </summary>
        /// <param name="prefix">Prefex to use for XML file names.</param>
        /// <param name="npcTemplate">
        /// Name template. Only NPCs whose name contains this template string will be processed.
        /// </param>
        public static void GenerateProbabilities(string prefix, string npcTemplate = null)
        {
            prefix = string.IsNullOrEmpty(prefix) ? GetModName() : prefix;

            var npcEntityClasses = GetNPCEntityClasses(npcTemplate);

            var weapons = BuildWeapons();
            WeaponAvgNormalized = NormalizeAverageWeaponScores(weapons);
            if (Debug)
            {
                LogAll(weapons.OrderBy(c => c.Score));
                DebugLog($"Normalized average weapon score: {WeaponAvgNormalized}");
            }

            var characters = BuildCharacters(npcEntityClasses, weapons);
            if (Debug)
            {
                LogAll(characters.OrderBy(c => c.PrefabName).ThenBy(c => c.Difficulty));
            }

            var allFactions = BuildFactions(characters);
            var enemyFaction = CombineHumanFactions(allFactions, true);
            var friendlyFaction = CombineHumanFactions(allFactions, false);

            var biomeGroups = CalculateAllBiomeGroups(enemyFaction, friendlyFaction, prefix);

            var prefabGroups = CalculateAllPrefabGroups(allFactions, enemyFaction, friendlyFaction);

            var outputDirectory = GetOutputDirectory();

            WriteXMLFiles(biomeGroups, prefabGroups, outputDirectory, prefix);

            Log.Out("Files output to: {0}", outputDirectory);

            GameIO.OpenExplorer(outputDirectory);
        }

        private static List<BiomeSpawnGroupInfo> BuildBiomeSpawnGroups(
            List<CharacterInfo> characters,
            bool isEnemyGroup,
            string prefix = "")
        {
            var biomes = new[] { "pine_forest", "burnt_forest", "desert", "snow", "wasteland" };

            var times = new[] { "Day", "Night" };

            var regions = new[] { string.Empty, "Downtown" };

            return biomes
                .SelectMany((biome, b) =>
                {
                    return times.SelectMany((time, t) =>
                    {
                        return regions.Select((region, r) =>
                        {
                            var groupType = isEnemyGroup ? "Enemy" : "Friendly";
                            var isNight = t == 1;
                            var biomeTags = FastTags<TagGroup.Global>.Parse(biome);
                            return new BiomeSpawnGroupInfo
                            {
                                Biome = biome,
                                BiomeDifficulty = 10 * b,
                                Difficulty = (10 * b + 33 * t + 25 * r),
                                EntityGroup = $"{prefix}{groupType}{ToPascalCase(biome)}{region}{time}",
                                IsEnemy = isEnemyGroup,
                                IsNight = isNight,
                                IsDowntownOnly = r == 1,
                                Tags = biomeTags,
                                Characters = CharacterInfo.Clone(
                                    characters.Where(c => CanSpawnInBiome(c, biomeTags, isNight)))
                            };
                        });
                    });
                })
                .OrderBy(np => np.Difficulty)
                .ToList();
        }

        private static List<CharacterInfo> BuildCharacters(
            List<EntityClass> npcEntityClasses,
            List<WeaponInfo> weaponInfos)
        {
            var gameModeForId = GameMode.GetGameModeForId(GameStats.GetInt(EnumGameStats.GameModeId));
            var propItemsOnEnterGame = EntityClass.PropItemsOnEnterGame + "." + gameModeForId.GetTypeName();

            return npcEntityClasses
                .Select(ec =>
                {
                    // <passive_effect name="HealthMax" operation="base_set" value="[health]"/>
                    var health = GetPassiveEffectValue(
                        ec.Effects.EffectGroups,
                        PassiveEffects.HealthMax,
                        300);
                    
                    // <passive_effect name="PhysicalDamageResist" operation="base_set" value="[damage resistance]"/>
                    var damageResist = GetPassiveEffectValue(
                        ec.Effects.EffectGroups,
                        PassiveEffects.PhysicalDamageResist,
                        10);
                    // Add additional damage resistance from the "DR" tags, if any.
                    damageResist += GetAdditionaDamageResistance(ec);

                    // Some NPCs use ItemOnEnterGame, some use HandItem
                    var itemsOnEnterGame = ec.Properties.GetString(propItemsOnEnterGame).Split(',');
                    var handItem = ec.Properties.GetString("HandItem");

                    return new CharacterInfo
                    {
                        ClassName = ec.entityClassName,
                        PrefabName = ec.meshPath.Substring(ec.meshPath.LastIndexOf('?') + 1),
                        EntityClass = ec,
                        DamageResist = damageResist,
                        Health = health,
                        Faction = ec.Properties.GetString("Faction"),
                        Weapon = weaponInfos.FirstOrDefault(wi => itemsOnEnterGame.Contains(wi.Name)) ??
                                 weaponInfos.FirstOrDefault(wi => wi.Name.EqualsCaseInsensitive(handItem))
                    };
                })
                // Add a default character per weapon, with values taken from npcMeleeTemplate
                .Concat(weaponInfos.Select(weaponInfo => new CharacterInfo
                {
                    ClassName = BASELINE_CHARACTER,
                    DamageResist = 30f,
                    Health = 300,
                    // Leave the faction null so it gets added to all factions
                    Weapon = weaponInfo
                }))
                .ToList();
        }

        private static List<FactionInfo> BuildFactions(IEnumerable<CharacterInfo> characters)
        {
            if (!(FactionManager.Instance?.Factions?.Length > 0))
            {
                throw new ArgumentException("Factions not initialized!");
            }

            var whiteriverId = FactionManager.Instance.GetFactionByName("whiteriver").ID;

            return FactionManager.Instance.Factions
                .Where(faction => faction != null)
                .Select(faction => new FactionInfo
                {
                    Name = faction.Name,
                    AllCharacters = characters
                            .Where(characterInfo =>
                                // Always add the baseline character.
                                characterInfo.ClassName == BASELINE_CHARACTER ||
                                string.IsNullOrEmpty(characterInfo.Faction) ?
                                    // If they don't have a faction, put them in the bandits or
                                    // whiteriver faction, depending upon whether they're an enemy.
                                    (characterInfo.EntityClass != null &&
                                    characterInfo.EntityClass.Properties.GetBool("IsEnemyEntity") ?
                                        faction.Name == "bandits" :
                                        faction.Name == "whiteriver") :
                                    characterInfo.Faction.EqualsCaseInsensitive(faction.Name))
                            .Select(CharacterInfo.Clone)
                            .ToList(),
                    // It's an enemy faction if it hates Whiteriver.
                    IsEnemy = faction.GetRelationship(whiteriverId) < 200f,
                    IsHuman = HumanFactions.Contains(faction.Name.ToLower())
                })
                // Remove factions that only have the baseline character or are non-human
                // (maybe we can do non-human NPCs later, but not right now).
                .Where(factionInfo =>
                    factionInfo.IsHuman &&
                    factionInfo.AllCharacters?.Any(fi => fi.ClassName != BASELINE_CHARACTER) == true)
                .ToList();
        }

        private static void BuildFactionSpawnGroupsAll(
            FactionInfo faction,
            bool hasMultipleFactions)
        {
            // The "All" groups are interspersed with the "Boss" groups.
            // This is so the "Boss" group difficulties can be offest, such that a GS01 Boss group
            // will be between the GS100 All and GS200 All groups in terms of difficulty.
            var isBossType = new[] {false, true};

            faction.All = isBossType
                .SelectMany(isBoss =>
                {
                    return Gamestages.Select((gamestage, g) =>
                    {
                        var type = isBoss ? "Boss" : "All";
                        var factionGroupName = ToFactionGroupName(faction.Name);
                        return new EntityGroupInfo
                        {
                            EntityGroup = $"npc{factionGroupName}{type}GroupGS{gamestage:00}",
                            Difficulty = GetPrefabGroupDifficulty(g, isBoss),
                            IsBoss = isBoss,
                            IsEnemy = faction.IsEnemy,
                            HasMultipleFactions = hasMultipleFactions,
                            Characters = CharacterInfo.Clone(faction.AllCharacters)
                        };
                    });
                })
                .OrderBy(eg => eg.Difficulty)
                .ToList();
        }

        private static void BuildFactionSpawnGroupsMeleeRanged(FactionInfo faction)
        {
            faction.Melee = faction.All
                .Where(ec => !ec.IsBoss)
                .Select(ec =>
                {
                    var groupName = ec.EntityGroup.Replace("All", "Melee");
                    return new EntityGroupInfo
                    {
                        EntityGroup = groupName,
                        Difficulty = ec.Difficulty,
                        IsBoss = false,
                        IsEnemy = faction.IsEnemy,
                        HasMultipleFactions = ec.HasMultipleFactions,
                        Characters = CharacterInfo.Clone(ec.Characters.Where(c => c.Weapon?.IsRanged == false))
                    };
                })
                .ToList();

            faction.Ranged = faction.All
                .Where(ec => !ec.IsBoss)
                .Select(ec =>
                {
                    var groupName = ec.EntityGroup.Replace("All", "Ranged");
                    return new EntityGroupInfo
                    {
                        EntityGroup = groupName,
                        Difficulty = ec.Difficulty,
                        IsBoss = false,
                        IsEnemy = faction.IsEnemy,
                        HasMultipleFactions = ec.HasMultipleFactions,
                        Characters = CharacterInfo.Clone(ec.Characters.Where(c => c.Weapon?.IsRanged == true))
                    };
                })
                .ToList();
        }

        private static List<WeaponInfo> BuildWeapons()
        {
            // Note: I am using vanilla ammo damages for these weapons. I am working under the
            // assumption that NPC weapons should have the same relative "dangerousness" as vanilla
            // weapons, even if NPC Core hasn't implemented it that way, yet.

            return new List<WeaponInfo> {
                new WeaponInfo
                {
                    Name = "meleeNPCEmptyHand",
                    // NPC Core: 10
                    Damage = 7, // meleeWpnKnucklesT0LeatherKnuckles
                    BurstCount = 1,
                    IsRanged = false
                },
                new WeaponInfo
                {
                    Name = "meleeNPCAxe",
                    // NPC Core: 20
                    Damage = 19, // meleeToolAxeT1IronFireaxe + Power Attack
                    BurstCount = 1,
                    IsRanged = false
                },
                new WeaponInfo
                {
                    Name = "meleeNPCBatWood",
                    // NPC Core: 20
                    Damage = 20, // meleeWpnClubT1BaseballBat
                    BurstCount = 1,
                    IsRanged = false
                },
                new WeaponInfo
                {
                    Name = "meleeNPCClubWood",
                    // NPC Core: 15
                    Damage = 12, // meleeWpnClubT0WoodenClub
                    BurstCount = 1,
                    IsRanged = false
                },
                new WeaponInfo
                {
                    Name = "meleeNPCKnife",
                    // NPC Core: 20
                    Damage = 18, // meleeWpnBladeT1HuntingKnife + Power Attack
                    BurstCount = 1,
                    IsRanged = false
                },
                new WeaponInfo
                {
                    Name = "meleeNPCMachete",
                    // NPC Core: 20
                    Damage = 25, // meleeWpnBladeT3Machete
                    BurstCount = 1,
                    IsRanged = false
                },
                new WeaponInfo
                {
                    Name = "meleeNPCSpear",
                    // NPC Core: 20
                    Damage = 20, // meleeWpnSpearT1IronSpear
                    BurstCount = 1,
                    IsRanged = false
                },
                new WeaponInfo
                {
                    Name = "gunNPCAk47",
                    // NPC Core: 10
                    Damage = 47, // ammo762mmBulletBall
                    BurstCount = 5,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCAShotgun",
                    // NPC Core: 5
                    Damage = 10, // ammoShotgunShell
                    BurstCount = 4,
                    RayCount = 10,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCDPistol",
                    // NPC Core: 5 - this is a mistake, should be 10
                    Damage = 70, // ammo44MagnumBulletBall
                    BurstCount = 4,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCHRifle",
                    // NPC Core: 10
                    Damage = 57, // ammo762mmBulletAP
                    BurstCount = 1,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCLBow",
                    // NPC Core: 10
                    Damage = 38, // ammoArrowIron
                    BurstCount = 1,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCM60",
                    // NPC Core: 10
                    Damage = 47, // ammo762mmBulletBall
                    // NPC Core: 10 - but that is way too high for these calculations
                    BurstCount = 7,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCPipeMG",
                    // NPC Core: 5
                    Damage = 32, // ammo9mmBulletBall
                    BurstCount = 5,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCPipePistol",
                    // NPC Core: 5
                    Damage = 32, // ammo9mmBulletBall
                    BurstCount = 3,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCPipePistol2",
                    // NPC Core: 5
                    Damage = 32, // ammo9mmBulletBall
                    BurstCount = 3,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCPipeRifle",
                    // NPC Core: 10
                    Damage = 47, // ammo762mmBulletBall
                    BurstCount = 1,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCPipeShotgun",
                    // NPC Core: 5
                    Damage = 10, // ammoShotgunShell
                    BurstCount = 1,
                    RayCount = 10,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCPistol",
                    // NPC Core: 5
                    Damage = 32, // ammo9mmBulletBall
                    BurstCount = 7,
                    IsRanged = true
                },
                // Using as default for ranged mechs
                new WeaponInfo
                {
                    Name = "gunNPCPistolHidden",
                    // NPC Core: 5
                    Damage = 32, // ammo9mmBulletBall
                    BurstCount = 7,
                    IsRanged = true
                },
                // Using as default for ranged mechs
                new WeaponInfo
                {
                    Name = "gunNPCPistolHiddenFlying",
                    // NPC Core: 5
                    Damage = 32, // ammo9mmBulletBall
                    BurstCount = 7,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCPShotgun",
                    // NPC Core: 5
                    Damage = 10, // ammoShotgunShell
                    BurstCount = 2,
                    RayCount = 10,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCRocketLauncher",
                    // This is roughly the max damage before it totally throws off the curve.
                    // NPC Core: 140
                    Damage = 375,
                    BurstCount = 1,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCSMG5",
                    // NPC Core: 5
                    Damage = 32, // ammo9mmBulletBall
                    // NPC Core: 10 - but that is too high for these calculations
                    BurstCount = 8,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCSRifle",
                    // NPC Core: 10
                    Damage = 57, // ammo762mmBulletAP
                    BurstCount = 3,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCTRifle",
                    // NPC Core: 10
                    Damage = 47, // ammo762mmBulletBall
                    // NPC Core: 10 - but that is way too high for these calculations
                    BurstCount = 6,
                    IsRanged = true
                },
                new WeaponInfo
                {
                    Name = "gunNPCXBow",
                    // NPC Core: 10
                    Damage = 59, // ammoCrossbowBoltIron
                    BurstCount = 1,
                    IsRanged = true
                }
            };
        }

        private static List<BiomeSpawnGroupInfo> CalculateAllBiomeGroups(
            List<FactionInfo> enemyFaction,
            List<FactionInfo> friendlyFaction,
            string prefix)
        {
            var biomeGroups = new List<BiomeSpawnGroupInfo>();

            // "Friendly" and "Enemy" entities should have their own biome groups
            if (friendlyFaction?.Count > 0)
            {
                biomeGroups.AddRange(CalculateBiomeSpawnsForFaction(friendlyFaction[0], prefix));
            }

            if (enemyFaction?.Count > 0)
            {
                biomeGroups.AddRange(CalculateBiomeSpawnsForFaction(enemyFaction[0], prefix));
            }

            return biomeGroups;
        }

        private static List<EntityGroupInfo> CalculateAllPrefabGroups(
            List<FactionInfo> allFactions,
            List<FactionInfo> enemyFaction,
            List<FactionInfo> friendlyFaction)
        {
            // Per-faction prefab entity groups
            var prefabGroups = CalculatePrefabSpawnsForFactions(allFactions, false);

            // Catch-all "friendly" and "enemy" prefab entity groups
            if (friendlyFaction?.Count > 0)
            {
                prefabGroups.AddRange(CalculatePrefabSpawnsForFactions(friendlyFaction, true));
            }

            if (enemyFaction?.Count > 0)
            {
                prefabGroups.AddRange(CalculatePrefabSpawnsForFactions(enemyFaction, true));
            }

            return prefabGroups
                .OrderBy(eg => eg.HasMultipleFactions)
                .ThenBy(eg => !eg.IsEnemy)
                // Trim the gamestage number off the end of the group name and sort by that.
                .ThenBy(eg => eg.EntityGroup.TrimEnd(new[] {'0', '1', '2', '4', '5', '8'}))
                // Now sort by difficulty - this puts "GS50" before "GS200" &etc.
                .ThenBy(eg => eg.Difficulty)
                .ToList();
        }

        private static List<BiomeSpawnGroupInfo> CalculateBiomeSpawnsForFaction(
            FactionInfo faction,
            string prefix)
        {
            var biomeGroups = BuildBiomeSpawnGroups(faction.AllCharacters, faction.IsEnemy, prefix);

            CalculateProbabilities(
                biomeGroups.OfType<EntityGroupInfo>().ToList(),
                // The hardest biome should be roughly 2/3 as difficult as the hardest prefab group.
                // (This won't be exact because we are adding an integer number of groups.)
                (biomeGroups.Count * 2) / 3);

            biomeGroups =
                RemoveAllCharacters(
                    biomeGroups,
                    c => c.ClassName != BASELINE_CHARACTER)?
                .ToList();

            NormalizeGroups(biomeGroups, BIOME_PROB);

            return biomeGroups
                .OrderBy(np => np.IsEnemy)
                .ThenBy(np => np.BiomeDifficulty)
                .ThenBy(np => np.IsDowntownOnly)
                .ThenBy(np => np.IsNight)
                .ToList();
        }

        private static List<EntityGroupInfo> CalculatePrefabSpawnsForFactions(
            List<FactionInfo> factions,
            bool hasMultipleFactions)
        {
            var prefabGroups = new List<EntityGroupInfo>();

            foreach (var faction in factions)
            {
                // Calculate the normalized probabilities for the "All" groups
                // (which have both ranged and melee characters).
                BuildFactionSpawnGroupsAll(faction, hasMultipleFactions);
                CalculateProbabilities(faction.All);
                faction.All = RemoveUnusedPrefabCharacters(faction);

                // Copy the characters, and probabilities, into the "Ranged" and "Melee" groups.
                // You should have the same probability of encountering a particular ranged/melee
                // character in the "All" group as in the "Ranged"/"Melee" group at the same GS.
                BuildFactionSpawnGroupsMeleeRanged(faction);

                // Normalize the "All", "Melee", and "Ranged" groups separately.
                NormalizeGroups(faction.All, PROB_TOTAL);
                NormalizeGroups(faction.Melee, PROB_TOTAL);
                NormalizeGroups(faction.Ranged, PROB_TOTAL);

                // Add them all to the list of entity groups. Sorting is done later.
                prefabGroups.AddRange(faction.All);
                prefabGroups.AddRange(faction.Melee);
                prefabGroups.AddRange(faction.Ranged);
            }

            return prefabGroups;
        }

        private static void CalculateProbabilities(
            List<EntityGroupInfo> entityGroups,
            int additionalTiers = 0)
        {
            if (!(entityGroups?.Count > 0))
                return;

            // At this point, all characters should be in all entity groups, so we don't need to
            // calculate the min and max difficulty scores individually per entity group.
            var minScore = entityGroups[0].Characters.Min(c => c.Difficulty);
            var range = entityGroups[0].Characters.Max(c => c.Difficulty) - minScore;

            // Add additional tiers. This is for biome spawns, since even the most difficult biome
            // spawns should not be as difficult as a boss volume at gamestage 800.
            var tiers = entityGroups.Count + additionalTiers;

            // The entity groups should already be in ascending order of difficulty.
            for (int t = 0; t < entityGroups.Count; t++)
            {
                // Normalized tier: 0..1
                var tierNorm = (float)t / (tiers - 1);
                // Tier ratio: -1.55 to 1.95, min tier to max tier.
                // We use somethong other than 1 to eliminate the characters with the lowest scores
                // from the most difficult entity groups, and vice versa.
                // These specific values were found through experimentation.
                var tierRatio = 3.5f * tierNorm - 1.55f;

                DebugLog($"[{t + 1}] {entityGroups[t].EntityGroup} ratio: {tierRatio}");

                foreach (var npc in entityGroups[t].Characters)
                {
                    // Normalized difficulty score: 0..1
                    var difficultyNorm = (npc.Difficulty - minScore) / range;
                    // A line that rotates around (avg, avg), slope determined by tierNorm;
                    // the probability is the y-value on that line when x = difficultyNorm.
                    // We use (avg, avg) rather than (0.5, 0.5) because NPC and weapon difficulties
                    // tend to group together in the lower difficulty values, and having the line
                    // rotate around the average means the groups will be more balanced overall.
                    var avg = WeaponAvgNormalized;
                    var prob = (tierRatio * difficultyNorm) - (tierRatio * avg) + avg;
                    // Make sure it's in the range of [0,1]
                    npc.Probability = Mathf.Clamp01(prob);
                }

                // Remove the characters with zero probability of spawning.
                entityGroups[t].Characters =
                    entityGroups[t].Characters.Where(c => c.Probability > 0).ToList();
            }
        }

        private static bool CanSpawnInBiome(
            CharacterInfo character,
            FastTags<TagGroup.Global> biomeTags,
            bool isNight)
        {
            if (character.EntityClass?.Tags == null)
                return true;

            if (character.EntityClass.Tags.Test_AnySet(NoBiomeTags))
                return false;

            // Only spawn characters if they have this biome in their tags,
            // or if they have NO biomes in their tags ("all biomes").
            var biomeOk =
                character.EntityClass.Tags.Test_AnySet(biomeTags) ||
                !character.EntityClass.Tags.Test_AnySet(AllBiomeTags);

            // Don't spawn in the dayime if they have the "night" tag, and vice versa.
            var timeOk = isNight ?
                !character.EntityClass.Tags.Test_AnySet(DayTags) :
                !character.EntityClass.Tags.Test_AnySet(NightTags);

            if (!(timeOk && biomeOk))
            {
                var strTime = isNight ? "at night" : "during the day";
                DebugLog($@"{character.ClassName} can NOT spawn into {biomeTags} {strTime}");
            }

            return timeOk && biomeOk;
        }

        private static List<FactionInfo> CombineHumanFactions(List<FactionInfo> factions, bool isEnemy)
        {
            return factions?
                .GroupBy(faction => faction.IsEnemy == isEnemy)
                .Where(group => group.Key)
                .Select(group =>
                {
                    var groupName = isEnemy ? "Enemy" : "Friendly";

                    var characters = new List<CharacterInfo>();
                    foreach (var faction in group)
                    {
                        if (!faction.IsHuman)
                            continue;
                        if (!(faction.AllCharacters?.Count > 0))
                            continue;
                        DebugLog($"Adding {faction.AllCharacters.Count} characters from {faction.Name} to {groupName} group");
                        characters.AddRange(CharacterInfo.Clone(faction.AllCharacters));
                    }

                    return new FactionInfo
                    {
                        Name = groupName,
                        IsEnemy = isEnemy,
                        IsHuman = true,
                        AllCharacters = characters
                    };
                })
                .Where(factionInfo => factionInfo?.AllCharacters?.Count > 0)
                .ToList();
        }

        // NPC Core has a feature where entities with "DR" tags get additional damage resistance.
        private static float GetAdditionaDamageResistance(EntityClass ec)
        {
            if (ec.Tags.Test_AnySet(DRLow))
                return 10;
            if (ec.Tags.Test_AnySet(DRMid))
                return 20;
            if (ec.Tags.Test_AnySet(DRHigh))
                return 30;
            return 0;
        }

        private static string GetModName()
        {
            foreach (Mod mod in ModManager.GetLoadedMods())
            {
                if (mod.Name.StartsWith("1-") &&
                    SdFile.Exists(mod.Path + "/Config/entityclasses.xml"))
                {
                    return mod.Name.TrimStart(new char[] { '1', '-' });
                }
            }
            return "unknownNpcPack";
        }

        private static List<EntityClass> GetNPCEntityClasses(string npcTemplate)
        {
            if (string.IsNullOrEmpty(npcTemplate))
            {
                // Return all non-template, non-trader entities with the NPC tag
                return EntityClass.list.Dict.Values
                    .Where(ec =>
                        ec.Tags.Test_AnySet(NpcTags) &&
                        !ec.entityClassName.Contains("Template") &&
                        !ec.entityClassName.Contains("Trader"))
                    .ToList();
            }

            DebugLog($"Retrieving entity classes whose name contains \"{npcTemplate}\"");

            return EntityClass.list.Dict.Values
                .Where(ec => ec.entityClassName.ContainsCaseInsensitive(npcTemplate))
                .ToList();
        }

        private static string GetOutputDirectory()
        {
            if (ConnectionManager.Instance.IsServer)
            {
                return GameIO.GetSaveGameDir() + "/ConfigsDump";
            }
            return GameIO.GetSaveGameLocalDir() + "/ConfigsDump";
        }

        // <passive_effect name="[effectType]" operation="base_set" value="[return value]"/>
        private static float GetPassiveEffectValue(
            List<MinEffectGroup> effectGroups,
            PassiveEffects effectType,
            float defaultValue)
        {
            if (!(effectGroups?.Count > 0))
                return defaultValue;

            foreach (var eg in effectGroups)
            {
                var passiveEffect =
                    eg.PassiveEffects.FirstOrDefault(pe =>
                        pe.Type == effectType &&
                        pe.Modifier == PassiveEffect.ValueModifierTypes.base_set);
                
                if (!(passiveEffect?.Values?.Length > 0))
                    continue;

                return passiveEffect.Values[0];
            }

            return defaultValue;
        }

        private static int GetPrefabGroupDifficulty(int gsIndex, bool isBossGroup)
        {
            if (isBossGroup)
            {
                return 1 + (gsIndex + 2 >= Gamestages.Length ?
                    Gamestages[gsIndex] * 2 :
                    Gamestages[gsIndex + 2]);
            }

            return Gamestages[gsIndex];
        }

        private static void LogAll<T>(IEnumerable<T> list)
        {
            if (!list.Any())
                return;

            foreach (var item in list)
            {
                DebugLog(item.ToString());
            }
        }

        private static float NormalizeAverageWeaponScores(List<WeaponInfo> weapons)
        {
            var min = weapons.Min(w => w.Score);
            var range = weapons.Max(w => w.Score) - min;

            // Sum of the normalized weapon scores, [0..1]
            var sum = weapons.Sum(w => (w.Score - min) / range);

            return sum / weapons.Count();
        }

        private static void NormalizeGroups<T>(
            List<T> entityGroups,
            float targetTotal)
            where T : EntityGroupInfo
        {
            if (!(entityGroups?.Count > 0))
                return;

            foreach (var entityGroup in entityGroups)
            {
                if (!(entityGroup.Characters?.Count > 0))
                    continue;

                var egTotal = entityGroup.Characters.Aggregate(
                    0f,
                    (sum, character) => sum + character.Probability);

                if (egTotal == 0f)
                    continue;

                foreach (CharacterInfo cinfo in entityGroup.Characters)
                {
                    // Normalize so the total caracter probabilities are in the range of [0..1],
                    // and multiply by the target total for a final range of [0..target].
                    // But multiply first, to avoid rounding errors with small float values.
                    cinfo.Probability = (cinfo.Probability * targetTotal) / egTotal;
                }

                // Remove characters with very low probabilities (they will show up with zero
                // probability in the XML after rounding).
                entityGroup.Characters =
                    entityGroup.Characters.Where(c => c.Probability > 0.009f).ToList();
            }
        }

        private static IEnumerable<T> RemoveAllCharacters<T>(
            IEnumerable<T> entityGroups,
            Func<CharacterInfo, bool> predicate)
            where T : EntityGroupInfo
        {
            //if (entityGroups?.Any() != true)
            //    return entityGroups;

            return entityGroups?
                .Where(eg =>
                {
                    eg.Characters = eg.Characters?.Where(predicate).ToList();

                    return eg.Characters?.Count > 0;
                });
        }

        private static IEnumerable<T> RemoveNonBossCharacters<T>(IEnumerable<T> entityGroups)
            where T : EntityGroupInfo
        {
            if (entityGroups?.Any() != true)
                return entityGroups;

            // If none of the characters are tagged with "boss" tags, then do nothing.
            var hasBossCharacters = entityGroups
                .Any(eg => eg.Characters?
                    .Where(c => c.EntityClass.Tags.Test_AnySet(BossTags))
                    .Any() == true);

            if (!hasBossCharacters)
                return entityGroups;

            foreach (var eg in entityGroups)
            {
                if (eg.IsBoss)
                {
                    // Remove non-boss characters from boss groups
                    eg.Characters = eg.Characters?
                        .Where(c => c.EntityClass.Tags.Test_AnySet(BossTags))
                        .ToList();
                }
                else
                {
                    // Remove boss characters from non-boss groups
                    eg.Characters = eg.Characters?
                        .Where(c => !c.EntityClass.Tags.Test_AnySet(BossTags))
                        .ToList();
                }
            }

            return entityGroups;
        }

        private static List<EntityGroupInfo> RemoveUnusedPrefabCharacters(FactionInfo faction)
        {
            var nonBaselineCharacters = RemoveAllCharacters(
                faction.All,
                c => c.ClassName != BASELINE_CHARACTER);

            var nonPrefabCharacters = RemoveAllCharacters(
                nonBaselineCharacters,
                c => !c.EntityClass.Tags.Test_AnySet(NoPrefabTags));

            return RemoveNonBossCharacters(nonPrefabCharacters).ToList();
        }

        private static string ToFactionGroupName(string name)
        {
            switch (name?.ToLower())
            {
                // Sample military factions from NPC Core
                case "redteam":
                case "blueteam":
                case "greenteam":
                    return "Military";
                // Mechs - the faction group name is singular "Mech", not plural "Mechs"
                case "mechs":
                    return "Mech";
                // Animals - still a WIP that needs more testing
                case "aggressiveanimals":
                case "aggressiveanimalssmall":
                case "aggressiveanimalsmedium":
                case "aggressiveanimalslarge":
                    return "EnemyAnimals";
                case "companionanimals":
                case "passiveanimalssmall":
                case "passiveanimalsmedium":
                case "passiveanimalslarge":
                    return "FriendlyAnimals";
                case "animals":
                    // Assume that custom animals in the vanilla "animals" faction are enemies.
                    // NPC Pack authors usually use one of the NPC Core animal factions anyway.
                    return "EnemyAnimals";
                default:
                    return ToPascalCase(name);
            }
        }

        private static string ToPascalCase(string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            var sentence = s.ToLower().Replace("_", " ");
            TextInfo info = CultureInfo.CurrentCulture.TextInfo;
            return info.ToTitleCase(sentence).Replace(" ", string.Empty);
        }

        private static void WriteEntityGroupsXML(
            List<BiomeSpawnGroupInfo> biomeGroups,
            List<EntityGroupInfo> prefabGroups,
            string outputDirectory,
            string prefix)
        {
            var xml = new StringBuilder(XML_DTD);
            xml.AppendLine("<configs>");

            // Biome spawn groups
            xml.AppendLine($"    <insertAfter xpath=\"/entitygroups/entitygroup[last()]\">");
            foreach(var biomeGroup in biomeGroups)
            {
                if (!(biomeGroup?.Characters?.Count > 0))
                    continue;

                xml.AppendLine($"        <entitygroup name=\"{biomeGroup.EntityGroup}\">");
                foreach(var character in biomeGroup.Characters)
                {
                    // Biome spawns have lower probabilities, format to more decimal places.
                    xml.AppendLine($"            {character.ClassName}, {character.Probability:.000}");
                }
                // "None" value, to prevent too many from spawining.
                var noneProb = PROB_TOTAL - BIOME_PROB;
                xml.AppendLine($"            none, {noneProb}");
                
                xml.AppendLine("        </entitygroup>");
            }
            xml.AppendLine("    </insertAfter>\n");

            // NPC prefab spawn groups
            foreach (var prefabGroup in prefabGroups)
            {
                if (!(prefabGroup?.Characters?.Count > 0))
                    continue;

                xml.AppendLine($"    <csv xpath=\"/entitygroups/entitygroup[@name='{prefabGroup.EntityGroup}']/text()\" delim=\"\\n\" op=\"add\">");
                foreach (var character in prefabGroup.Characters)
                {
                    xml.AppendLine($"            {character.ClassName}, {character.Probability:.00}");
                }
                xml.AppendLine("    </csv>");
            }

            xml.AppendLine("</configs>");

            WriteXMLFile(xml.ToString(), outputDirectory, $"{prefix}-entitygroups.xml");
        }

        private static void WriteSpawningXML(
            List<BiomeSpawnGroupInfo> biomeGroups,
            string outputDirectory,
            string prefix)
        {
            var xml = new StringBuilder(XML_DTD);
            xml.AppendLine("<configs>");

            var biomeGroupings = biomeGroups.GroupBy(bg => bg.Biome);

            foreach (var biomeGrouping in biomeGroupings)
            {
                xml.AppendLine($"    <append xpath=\"/spawning/biome[@name='{biomeGrouping.Key}']\">");

                foreach (var biomeGroup in biomeGrouping.OrderBy(g => g.IsNight))
                {
                    var time = biomeGroup.IsNight ? "Night" : "Day";

                    if (biomeGroup.IsDowntownOnly)
                    {
                        xml.AppendLine($"        <spawn maxcount=\"1\" respawndelay=\"1\" time=\"{time}\" entitygroup=\"{biomeGroup.EntityGroup}\" tags=\"downtown\" />");
                    }
                    else
                    {
                        // In NPC Core, if a group is not downtown-only, it has two spawners: one
                        // for commercial and industrial districts, and one for everywhere outside
                        // commercial, industrial, and downtown districts.
                        xml.AppendLine($"        <spawn maxcount=\"1\" respawndelay=\"1\" time=\"{time}\" entitygroup=\"{biomeGroup.EntityGroup}\" notags=\"commercial,industrial,downtown\" />");
                        xml.AppendLine($"        <spawn maxcount=\"1\" respawndelay=\"1\" time=\"{time}\" entitygroup=\"{biomeGroup.EntityGroup}\" tags=\"commercial,industrial\" notags=\"downtown\" />");
                    }
                }

                xml.AppendLine("    </append>");
            }

            xml.AppendLine("</configs>");

            WriteXMLFile(xml.ToString(), outputDirectory, $"{prefix}-spawning.xml");
        }

        private static void WriteXMLFile(string xml, string outputDirectory, string filename)
        {
            if (!SdDirectory.Exists(outputDirectory))
            {
                SdDirectory.CreateDirectory(outputDirectory);
            }

            var xmlFile = new XmlFile(xml, outputDirectory, filename);

            xmlFile.SerializeToFile($"{outputDirectory}/{filename}");

            DebugLog($"Serialized {filename} to {outputDirectory}/{filename}");
        }

        private static void WriteXMLFiles(
            List<BiomeSpawnGroupInfo> biomeGroups,
            List<EntityGroupInfo> prefabGroups,
            string outputDirectory,
            string prefix)
        {
            WriteEntityGroupsXML(biomeGroups, prefabGroups, outputDirectory, prefix);

            WriteSpawningXML(biomeGroups, outputDirectory, prefix);
        }
    }
}
