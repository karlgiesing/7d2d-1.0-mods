﻿using NPCProbabilator.Scripts;
using System.Collections.Generic;

/// <summary>
/// In-game console command to generate NPC probabilities.
/// </summary>
public class ConsoleCmdGenerateNPCProbabilities : ConsoleCmdAbstract
{
    public override void Execute(List<string> _params, CommandSenderInfo _senderInfo)
    {
        var prefix = _params.Count > 0 ? _params[0] : null;
        var npcTemplate = _params.Count >  1 ? _params[1] : prefix;
        ProbabilityCalculator.GenerateProbabilities(prefix, npcTemplate);
    }

    public override string[] getCommands()
    {
        return new string[]
        {
            "generatenpcprobabilities",
            "gnp",
            "npcprobabilator"
        };
    }

    public override string getDescription()
    {
        return "Calculates NPC probabilities, and generates the required game config XML files.";
    }

    public override string getHelp()
    {
        return @"Generates NPC probabilities, and exports the required game config XML files.

Accepts a prefix as an argument, which is used when naming custom spawners and groups,
and is also used to prefix the names of the XML files themselves.

Also accepts an NPC name template as an argument, which is matched against the entity class name.
Only entity class names that contain the template string will be processed.
(This is just a plain string; wildcards are not accepted.)

For convenience, if only one argument is provided, this will be used as BOTH the prefix and the template.

XML files will be written to the ConfigsDump subdirectory of the save game.

Usage:
    generatenpcprobabilities <prefix> <template>";
    }
}
