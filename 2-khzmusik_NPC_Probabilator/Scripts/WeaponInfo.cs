﻿namespace NPCProbabilator.Scripts
{
    /// <summary>
    /// Information about the weapon wielded by an NPC. This is one of the things used to determine
    /// the difficulty of the NPC.
    /// </summary>
    public class WeaponInfo
    {
        /// <summary>
        /// The weapon name, as it appears in the "ItemsOnEnterGame" or "HandItem" property:
        /// "meleeNPCKnife", "gunNPCSMG5", etc.
        /// </summary>
        public string Name {  get; set; }

        /// <summary>
        /// The damage done by this weapon.
        /// For ranged weapons, this is base damage per ray that hits.
        /// This is taken from the <c>DamageEntity</c> property of <c>Action0</c> (melee)
        /// or <c>Action1</c> (ranged).
        /// </summary>
        public float Damage { get; set; }

        /// <summary>
        /// The burst count of this weapon. This is taken from the NPC Core <c>$Burstsize</c> cvar.
        /// </summary>
        public int BurstCount { get; set; }

        /// <summary>
        /// The number of rays that is generated per round when firing this weapon.
        /// Shotguns have multiple rays per round, each representing a "pellet" or "shot",
        /// and the number of rays that hit the target decreases dramatically by distance.
        /// This is taken from the <c>RoundRayCount</c> passive effect.
        /// </summary>
        public int RayCount { get; set; } = 1;

        /// <summary>
        /// When considering <see cref="RayCount"/>, use this ratio to determine how many rays
        /// ("pellets" or "shot") would typically hit the target. A higher ratio means that weapons
        /// with a higher ray count are considered that much more difficult.
        /// </summary>
        public float RayCountHitRatio => 0.66f;

        /// <summary>
        /// True if this weapon is ranged, false if it's melee.
        /// </summary>
        public bool IsRanged { get; set; }

        /// <summary>
        /// If a weapon is ranged, modify its score by this amount.
        /// </summary>
        public float RangedScoreModifier => 10f;

        /// <summary>
        /// The "score" for this weapon. A higher score means it is more dangerous.
        /// </summary>
        public float Score => Damage * BurstCount * (RayCount > 1 ? RayCount * RayCountHitRatio : 1) + (IsRanged ? RangedScoreModifier : 0f);

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{Name}: Score = {Score}";
        }
    }
}
