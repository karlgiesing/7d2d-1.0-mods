﻿namespace NPCProbabilator.Scripts
{
    public class BiomeSpawnGroupInfo : EntityGroupInfo
    {
        /// <summary>
        /// The name of the associated biome.
        /// </summary>
        public string Biome { get; set; }

        /// <summary>
        /// The biome difficulty.
        /// </summary>
        public float BiomeDifficulty { get; set; }

        /// <summary>
        /// The tags for this biome. They can be matched against the "Tags" property of an entity.
        /// </summary>
        public FastTags<TagGroup.Global> Tags { get; set; }

        /// <summary>
        /// True if this group should only be used for night time spawns.
        /// </summary>
        public bool IsNight { get; set; }

        /// <summary>
        /// True if this group should only be used for downtown spawns.
        /// </summary>
        public bool IsDowntownOnly { get; set; }
    }
}
