﻿using System.Collections.Generic;
using System.Text;

namespace NPCProbabilator.Scripts
{
    public class EntityGroupInfo
    {
        /// <summary>
        /// The name of the entity group.
        /// </summary>
        public string EntityGroup { get; set; }

        /// <summary>
        /// The difficulty (e.g. gamestage) of this entity group.
        /// </summary>
        public int Difficulty { get; set; }

        /// <summary>
        /// True if this entity group represents a boss group.
        /// </summary>
        public bool IsBoss { get; set; }

        /// <summary>
        /// True if this entity group should spawn enemies.
        /// </summary>
        public bool IsEnemy { get; set; }

        /// <summary>
        /// True if this entity group can spawn entities in multiple factions.
        /// The NPC Core "Enemy" or "Friendly" entity groups do this.
        /// </summary>
        public bool HasMultipleFactions { get; set; }

        /// <summary>
        /// The characters that should spawn into this entity group.
        /// </summary>
        public List<CharacterInfo> Characters { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder(EntityGroup);

            foreach (var character in Characters)
            {
                sb.AppendLine($"    {character}");
            }

            return sb.ToString();
        }
    }
}
