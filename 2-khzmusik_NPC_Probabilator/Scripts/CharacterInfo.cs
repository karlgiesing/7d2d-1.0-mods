﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NPCProbabilator.Scripts
{
    public class CharacterInfo
    {
        /// <summary>
        /// The entity class name of the character.
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// The Unity prefab name of the character. This can be used to determine which entities
        /// are the same character (but with different weapons).
        /// </summary>
        public string PrefabName { get; set; }

        /// <summary>
        /// The entity class for this character.
        /// </summary>
        public EntityClass EntityClass { get; set; }

        /// <summary>
        /// The character's max health.
        /// </summary>
        public float Health { get; set; }

        /// <summary>
        /// The character's "PhysicalDamageResist" value.
        /// </summary>
        public float DamageResist { get; set; }

        /// <summary>
        /// The name of the faction the character belongs to.
        /// </summary>
        public string Faction { get; set; }

        /// <summary>
        /// Information about the weapon wielded by the character.
        /// </summary>
        public WeaponInfo Weapon { get; set; }

        /// <summary>
        /// The probability of this character spawning into an entity group.
        /// </summary>
        public float Probability { get; set; }

        /// <summary>
        /// The multiplier to use when considering how much the <see cref="Health"/> value should
        /// influence the character difficulty.
        /// </summary>
        // Default health is 300; multiplier should result in an addition of 0 for this value.
        // Double the default health should result in a value of 100.
        public float HealthMultiplier => (Health - 300) * .33f;

        /// <summary>
        /// The multiplier to use when considering how much the <see cref="DamageResist"/> value
        /// should influence the character difficulty.
        /// </summary>
        // NPC Core max damage resist is 40; multiplier should result in a max of 100.
        public float DamageResistMultiplier => 100f / 40f;

        /// <summary>
        /// The multiplier to use when considering how much the <see cref="WeaponInfo.Score"/>
        /// value should influence the character difficulty.
        /// </summary>
        // This depends upon the current max weapon score, which right now is 375;
        // multiplier should result in a max of 200.
        public float WeaponScoreMultiplier => 200f / 375f;

        /// <summary>
        /// This character's "diffuculty level." This takes into account the character's
        /// <see cref="Health"/>, <see cref="DamageResist"/>, and <see cref="WeaponInfo.Score"/>.
        /// </summary>
        public float Difficulty => Math.Max(1f,
            (HealthMultiplier * Health) +
            (DamageResistMultiplier * DamageResist) +
            (WeaponScoreMultiplier * (Weapon?.Score ?? 0)));

        /// <summary>
        /// Clones the character info. This is a shallow copy, but most fields are value types.
        /// </summary>
        /// <param name="characterInfo"></param>
        /// <returns></returns>
        public static CharacterInfo Clone(CharacterInfo characterInfo)
        {
            if (characterInfo == null)
                return null;

            return new CharacterInfo
            {
                ClassName = characterInfo.ClassName,
                PrefabName = characterInfo.PrefabName,
                EntityClass = characterInfo.EntityClass,
                Health = characterInfo.Health,
                DamageResist = characterInfo.DamageResist,
                Faction = characterInfo.Faction,
                Weapon = characterInfo.Weapon,
                Probability = characterInfo.Probability
            };
        }

        /// <summary>
        /// Clones a list of character info. This uses the <see cref="Clone(CharacterInfo)"/>
        /// method on all objects in the list.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<CharacterInfo> Clone(List<CharacterInfo> list)
        {
            return list?.Select(Clone).ToList();
        }

        /// <summary>
        /// Clones an enumerable of character info. This uses the <see cref="Clone(CharacterInfo)"/>
        /// method on all objects in the enumeration.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static List<CharacterInfo> Clone(IEnumerable<CharacterInfo> enumerable)
        {
            return enumerable?.Select(Clone).ToList();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{ClassName} ({Faction}): Difficulty = {Difficulty} [Health = {Health} / DamageResist = {DamageResist} / {Weapon} : Health mod ({HealthMultiplier} * {Health}) + DamageResist mod ({DamageResistMultiplier} * {DamageResist}) + WeaponScore mod ({WeaponScoreMultiplier} * {(Weapon?.Score ?? 0)})]";
        }
    }
}
