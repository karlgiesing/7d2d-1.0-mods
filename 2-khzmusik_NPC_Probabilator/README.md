# NPC Probabilator

This mod creates an in-game console command that generates NPC entity group probabilities and spawners,
and writes them to XML files.

It is designed for NPC Pack authors, and modders that use those packs.
It probably will not be of much use to end users (players).

Once the probabilities are calculated, and the XML files are generated, the job of this mod is over.
So, there is no point keeping it installed afterwards.

* :x: **Not** EAC safe (clients or servers **must** disable Easy Anti-Cheat)

## Features

> :warning: This mod has currently been tested with _human_ NPCs.
> Mechs and animals _may_ work but have not been tested.
> Zombies are **not** supported.

This mod creates a new in-game console command called `generatenpcprobabilities`.
For convenience, you can also use `gnp`,
or `npcprobabilator` if you want to use the name of the mod.

It supports two arguments:

1. Prefix: This is a prefix to add to any biome entity groups,
    and it will also be added as a prefix to the XML file names.
    If no prefix is provided, the mod will try to guess one,
    based on the name of the first NPC Pack that is loaded.
2. Template: This is a "template" string that can be matched against entity class names.
    Only entity class names that _contain_ this template will be calculated.
    This is a plain string; no wildcards are supported.
    If no template is provided, the _prefix_ is used for both.
    If _no_ arguments are provided, _all_ NPCs will be calculated.

To get in-game help with the command, type `help generatenpcprobabilities`
(or an equivalent command name).

Once you issue the command, the relevant entity group probabilities are calculated,
the new biome spawn groups are generated,
and XML files are written.
The files will be `[prefix]-entitygroups.xml` and `[prefix]-spawning.xml`.

XML files are automatically placed into the `ConfigsDump` directory
(the same directory that will open if you issue the `exportcurrentconfigs` command).

This directory will automatically open when the command is finished.
The command may take a few seconds, so be patient.

> :warning: No automated tool is perfect.
> You will probably want to go through the XML files to make sure they generated what you want.
> You may also want to copy some of the XML for your own groups,
> for example if you want your characters to have their own dedicated POI groups.

> :information_source: Once the XML files are generated, and you are happy with the results,
> this mod is no longer needed.
> It can be removed at that point.
> There should be no risk to saved games.

### Example Uses

For these examples, we'll assume that the NPCs are raiders,
and they are authored by someone named Jim.

We'll say the name of the mod is `1-JimsAwesomeRaiders`.

Here are some XML snippets from `entityclasses.xml` showing how they might be named:

```xml
<!-- First character: Raider Alice -->
<entity_class name="raiderAliceByJimClub" extends="npcBanditClubTemplate">
    <!-- ...snip... -->
</entity_class>
<entity_class name="raiderAliceByJimKnife" extends="npcBanditKnifeTemplate">
    <!-- ...snip... -->
</entity_class>
<!-- ...more Alice raiders with more weapons -->

<!-- Second character: Raider Bob -->
<entity_class name="raiderBobByJimClub" extends="npcBanditClubTemplate">
    <!-- ...snip... -->
</entity_class>
<entity_class name="raiderBobByJimKnife" extends="npcBanditKnifeTemplate">
    <!-- ...snip... -->
</entity_class>
<!-- ...more Bob raiders with more weapons -->
```

**Typical use:**
> `generatenpcprobabilities jimsRaiders raider`

Files generated:
* `jimsRaiders-entitygroups.xml`
* `jimsRaiders-spawning.xml`

**Alternate command names:**
> `gnp jimsRaiders raider`

> `npcprobabilator jimsRaiders raider`

Files generated:
* `jimsRaiders-entitygroups.xml`
* `jimsRaiders-spawning.xml`

**To have both the prefix and template be "raider":**
> `generatenpcprobabilities raider`

Files generated:
* `raider-entitygroups.xml`
* `raider-spawning.xml`

**To only generate probabilities for the Alice characters:**
> `generatenpcprobabilities jimsRaiders Alice`

Files generated:
* `jimsRaiders-entitygroups.xml`
* `jimsRaiders-spawning.xml`

**To generate probabilities for _all loaded_ NPC Packs:**
> `generatenpcprobabilities`

To create a prefix, the command will look at the names of all the loaded mods.
Since by strong convention, NPC packs are named with the prefix "1-",
the command will look for the first mod with that prefix.
It will choose that as the template name, stripping out the prefix.

So, assuming Jim's mod is alphabetically the first with a "1-" prefix,
these files will be generated:
* `JimsAwesomeRaiders-entitygroups.xml`
* `JimsAwesomeRaiders-spawning.xml`

### What is generated

The XML that is generated is designed to be used with the NPC Mod system.
The spawners and entity groups match the ones in the `0-XNPCCore` mod.

In `entitygroups.xml`:

* Entity groups for biome spawns.
    These entity groups are mod-specific.
    They are _not_ the same groups that are in NPC Core,
    though they are modeled after those groups.
    Each biome has four spawners:
  * Day
  * Night
  * Downtown Day
  * Downtown Night
* Entity groups for prefab (POI) spawns.
    These _are_ the same prefab entity groups that are in NPC Core.
    Entity groups are generated:
  * Per weapon type ("All", "Ranged", "Melee")
  * Per faction ("bandits", "whiteriver", etc.)
    The faction is taken from the "Faction" property on each NPC's `entity_class`.
  * For general "Friend" and "Enemy" groups

In `spawning.xml`:

* Appends new spawners to each biome (one per biome spawn group).

### Entity Class XML Tags

By default, NPCs spawn in all biomes, at all times of day, and in all relevant POI groups.

You can customize this behavior by adding tags to the "Tags" property of an entity's `entity_class`.
The entity classes are defined in the `entityclasses.xml` file of the NPC Pack.

#### Biome Spawn Tags

If you want entities to only spawn in certain biomes,
add the name of those biomes to the "Tags" property.
The tag name should exactly match the name of the biome in `biomes.xml`.

You can also specify a tag named "nobiome" (or "nobiomes")
to prevent the entity from spawning in _any_ biomes.
In this case, the entity will only spawn in POIs.

Entities can also spawn in biomes only during the day, or only at night.
To only spawn during the day, add the "day" (or "daytime") tag.
To only spawn at night, add the "night" (or "nighttime") tag.

**Examples** (other tags are omitted for clarity):
```xml
<!-- Only spawn in the desert, at any time of day -->
<property name="Tags" value="desert" />

<!-- Only spawn in the pine forest, burnt forest, or desert (not the snow or wasteland), at any time of day -->
<property name="Tags" value="pine_forest,burnt_forest,desert" />

<!-- Only spawn during the day -->
<property name="Tags" value="day" />
            
<!-- Only spawn in the wasteland at night -->
<property name="Tags" value="wasteland,night" />

<!-- Don't spawn in any biomes (only spawn in POIs) -->
<property name="Tags" value="nobiome" />
```

#### Prefab (POI) Spawn Tags

For each faction, there are four "kinds" of spawn groups available to prefab designers:

* "All" - all characters
* "Boss" - boss characters
* "Melee" - melee characters
* "Ranged" - ranged characters

NPC Probabilator will automatically populate the "Melee" and "Ragned" groups,
by examining the weapons wielded by each character.

However, the "Boss" spawn groups are different.

By default, NPC Probabilator uses the same characters for "Boss" groups as it does for "All" groups.
The "Boss" groups are simply more difficult.
A gamestage 1 "Boss" group is between the gamestage 100 and gamestage 200 "All" groups, in terms of difficulty.

But if you have characters that are specifically boss characters, this is probably not what you want.

So, if _any_ character is tagged with the "boss" tag, NPC Probabilator will take that into account:

* _Only_ characters tagged with a "boss" tag will spawn into the "Boss" prefab groups.
* Characters tagged with a "boss" tag will _not_ spawn into the "All", "Melee", or "Ranged" prefab groups.

> :warning: Tagging characters with the "boss" tag will _not_ prevent them from spawning into biomes.
> If you want them to only spawn into "Boss" prefab groups, and not biomes, add the "nobiome" tag.

**Example:**
```xml
<!--
    "Crazy Jake" boss, refered to in the "buried treasure" quests.
    This character should only spawn into "Boss" volumes in bandit POIs.
    Since it should not spawn into biomes, we also add the "nobiome" tag.
-->
<entity_class name="raiderCrazyJakeByJimRocketL" extends="npcBMRocketLTemplate">
    <!-- ...other properties and tags omitted... -->
    <property name="Tags" value="boss,nobiome" />
</entity_class>
```

## Dependent and Compatible Modlets

Technically, this mod does not depend upon any other mods.

However, it is designed for NPC Packs that are created for the NPC Core character system.
NPC Packs are dependent upon these mods:

* `0-SCore`
* `0-XNPCCore`

You must also install the additional NPC Packs whose probabilities you wish to generate.

## Technical Details

This modlet uses C#.
EAC must be turned off.
