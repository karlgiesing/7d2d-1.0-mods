# Slow Build

Slows down building, and to a lesser extent crafting.

This encourages renovating prefabs rather than building structures from scratch,
for both horde bases and personal dwellings.
It also makes inventory management more challenging.
The fact that wood items take nails to craft will also discourage nerd poling.

* :heavy_check_mark: Server side only (clients do not need to install the mod)
* :heavy_check_mark: EAC safe (clients and servers do not need to disable Easy Anti-Cheat)

## Features

* Wood frames, shapes, and ladders now require nails to craft.
  Player storage boxes also require nails to craft (like large storage boxes did in A21).
  To adjust for this, players are now given a small stack of nails when starting,
  and the amount of nails in loot has been increased.
* Wooden furniture and containers have a chance to produce nails when destroyed with a tool.
  Any tool will do; the tool does _not_ need to be a harvesting tool (like a wrench).
  _Scrapping_ furniture (like wooden chairs) will _not_ produce nails.
* Full boxes of nails (100 count) have been added to the game.
  These boxes can be found in loot, or bought from traders.
  Players can also craft boxes from loose nails (and they do not need to be unlocked).
* The stack sizes for all shapes have been reduced to 100 (from 500).
* The stack sizes for natural resources (such as iron or clay) have been reduced from 6000 to 500.
  Stack sizes for cobblestone, cement, and concrete have been reduced from 1000 to 100.
  Bundles of resources have been updated to reflect the new stack sizes.
* Trees now take twice as long to grow, and yield at most one seed.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.
