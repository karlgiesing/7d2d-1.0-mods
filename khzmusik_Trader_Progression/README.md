# Trader Progression

Traders are no longer locked to specific biomes,
but the "Opening Trade Routes" quests still progress through different biomes.

* :heavy_check_mark: Server side only (clients do not need to install the mod)
* :heavy_check_mark: EAC safe (clients and servers do not need to disable Easy Anti-Cheat)

## Features

### Multiple trader types per biome

Traders are no longer locked to one specific biome.
However, there are still _some_ biome limitations - not every trader will spawn in every biome.

Here are the biomes that each trader can spawn into:

* Trader Rekt: pine forest, desert
* Trader Jen: pine forest, burnt forest, desert
* Trader Bob: burnt forest, desert, wasteland
* Trader Hugh: snow, wasteland
* Trader Joel: pine forest, snow, wasteland

If you want, you can also remove _all_ biome limitations from _all_ traders.
This is done in the `rwgmixer.xml` file - see below.

### "Opening Trade Routes" biome progression

The "Opening Trade Routes" quests still send players to progressively more difficult biomes.

They are, in order:

1. Pine forest (the initial "White River Citizen" quest)
2. Burnt forest
3. Desert
4. Snow
5. Wasteland

This matches the biome progression of the vanila game in 7D2D v1.0.

This is customizable in the `quests.xml` file - see below.

## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.
It does not contain any additional images or Unity assets.
It is safe to install only on the server.

### Customizing trader biomes

If you don't want traders to have **any** biome limitations, that is also possible.

Instead of the code that is in `rwgmixer.xml`, you can use this XML instead:

```xml
<remove xpath="//prefab_spawn_adjust[starts-with(@partial_name,'trader_')]" />
<insertBefore xpath="//prefab_spawn_adjust[@partial_name='sawmill_01']">
    <prefab_spawn_adjust tags="trader" bias="20" min_count="2" max_count="4" />
</insertBefore>
```

This code is in `rwgmixer.xml` but is currently commented out.

### Customizing "Opening Trade Routes" biome progression

The "Opening Trade Routes" quests can also go to more than one target biome.
This allows for _some_ biome progression, but it is not completely fixed.
(This is how this mod worked in Alpha 21.)

Instead of the quest progression code that is in `quests.xml`, you can use this instead:

```xml
<set xpath="//quest[contains(@id, 'nexttrader') and not(contains(@id, 'test'))]/objective[@type='Goto']/property[@name='biome_filter_type']/@value">ExcludeBiome</set>
<append xpath="//quest[@id='tier2_nexttrader']">
    <property name="biome_filter" value="desert,snow,wasteland" />
</append>
<append xpath="//quest[@id='tier3_nexttrader']">
    <property name="biome_filter" value="pine_forest,wasteland" />
</append>
<append xpath="//quest[@id='tier4_nexttrader']">
    <property name="biome_filter" value="burnt_forest,pine_forest" />
</append>
<append xpath="//quest[@id='tier5_nexttrader']">
    <property name="biome_filter" value="burnt_forest,desert,pine_forest" />
</append>
```

This code is in `quests.xml` but is currently commented out.

If you use the above code, make sure you comment out the existing biome-related XML code first
(the XML code following the `Biome progression for "Opening Trade Routes" quests` comment).

### About the POIs

In the XML for each POI, there are properties that prevent certain POIs from spawning next to each other.
The "ThemeTags" property defines "themes" that each POI may be a part of.
The "ThemeRepeatDistance" property specifies the minimum distance between POIs of the same theme.

In version 1.0, TFP decided that each trader should have their own distinct theme.
This means Trader Jen will not spawn near another Trader Jen,
but Trader Jen _may_ spawn near Trader Rekt.
This isn't an issue if a trader can only spawn into one biome,
but if two traders can spawn in the same biome, they tend to spawn next to each other.

The POIs in this mod are exact copies of the vanilla trader POIs,
except they all have an additional "ThemeTag" property value of "trader".
This means they will no longer spawn next to each other.
This is the **only** difference between these POIs and the vanilla trader POIs.

Normally, if a mod distributes POIs, the mod must be installed on both servers and clients.
But _this is not the case_ with this mod.
The custom POIs are _only_ used for world generation.

This is why this mod is server-side.
Once the world is generated, the vanilla trader POIs can be used instead.
Clients already have the vanilla POIs, so they don't need to install the ones in this mod.
