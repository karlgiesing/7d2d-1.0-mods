# Gas Shortage

Makes gasoline scarce, and more difficult to craft.

* :heavy_check_mark: Server side only (clients do not need to install the mod)
* :heavy_check_mark: EAC safe (clients and servers do not need to disable Easy Anti-Cheat)

## Features

* Gasoline cannot be harvested from vehicles, gas pumps, or anything else.
    (It can still be _looted_ from vehicles, gas pumps, or other containers.)
* Each oil shale yields one can of gas (not 2).
* Stacks of gasoline hold 1000 cans of gas (not 5000),
    and cost 800 cans of gas to produce (not 2000).
* Gasoline, and stacks of gasoline, are twice as expensive to buy (and sell for twice as much).
    The economic bundle size for gasoline is halved (50 instead of 100).
* Traders stock 500 - 1000 can of gas at a time (not 5,000 - 10,000).

## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.
It does not contain any additional images or Unity assets.
It is safe to install only on the server.

## Possible improvements

I thought it would be fun if players spawned with a 4x4 upon entering the game,
but without any gasoline,
so they would have to loot any cars and gas barrels they found in order to use it.
(I deliberately chose the 4x4 because it is a gas hog.)

I realize this is not a play style for everyone, so it is not enabled by default.

If you want to do this, un-comment the code in `entityclasses.xml`.
(It is the only code in that XML file, so it should be obvious.)

You could also have them spawn with a different vehicle if you like.
