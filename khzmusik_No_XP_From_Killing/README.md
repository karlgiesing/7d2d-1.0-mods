# No XP From Killing

Players do not get any experience points from killing zombies or any other entity.

As a result, the XP notification will not display when a zombie is killed.
You can no longer use that notification to determine if a zombie is really dead.

To make up for this, the amount of XP gained from all other sources is doubled.
During my test playthroughs, I found that roughly half of the XP that I gained was from killing,
so this should make progression roughly the same as it would be otherwise.

I created this mod to lessen the incentive for killing dangerous enemies,
so players will have as munc incentive to avoid them instead.
(It does not _entirely_ remove the incentive for killing enemies;
zombies still drop loot bags, and animals can be harvested.)

This steers the game away from being a first-person shooter,
and closer to being a survival game.

* :heavy_check_mark: Server side only (clients do not need to install the mod)
* :heavy_check_mark: EAC safe (clients and servers do not need to disable Easy Anti-Cheat)

## Features

* No XP from killing.
* No XP notification when entities die.
* New loading screen tip.
* The Night Stalker "Twilight Thief" book gives double XP at night,
    for _any_ source of XP (previously only from killing).

## Technical Details

This modlet uses XPath to modify XML files.
It does not have any custom C# code, so it should be compatible with EAC.
It does _not_ contain any new assets (such as images or models).
Servers should automatically push the XML modifications to their clients, so separate client
installation should _not_ be necessary.

Starting a new game should not be necessary, but is always recommended just in case.
