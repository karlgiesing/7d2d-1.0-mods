# TEMPLATE

* :x: Server side only (clients do not need to install the mod)
* :heavy_check_mark: EAC safe (clients and servers do not need to disable Easy Anti-Cheat)

## Features



## Dependent and Compatible Modlets



## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.

It does not contain any additional images or Unity assets.

It is safe to install only on the server.

It must be installed on the server and on all clients.

EAC must be turned off.
