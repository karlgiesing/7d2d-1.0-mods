# Explosions in 7D2D

## Explosion.ParticleIndex

Example:
```xml
<property name="Explosion.ParticleIndex" value="7"/>
```

The "value" property is an index in the C# `WorldStaticData.prefabExplosions` array.
This array contains the transforms of Unity game objects,
which often have vanilla scripts attached.

### Explosion by index

0. No explosion
1. (unused) Exploding acid barrel?
2. "zombieDemolition" entity class: Demo zombie explosion
3. (unused) Exploding acid barrel?
4. (unused) Exploding car?
5. "ammoRocketHE" item: Rocket explosion
6. "vehicle..." entity classes: Player vehicle explosions? Flames when broken?
7. "ammoProjectileZombieVomit" (and related) items: Acid hit
8. "zombieFatCop" entity class: Cop explosion (with gibs)
9. (unused) Exploding car?
10. "thrownAmmoMolotovCocktail" item
11. "mineCandyTin" block; "ammoArrowExploding", "ammoCrossbowBoltExploding", "thrownAmmoFlare" items
12. "thrownAmmoPipeBomb", "thrownTimedCharge" items
13. "thrownGrenadeContact", "thrownDynamite" items
14. "cntBarrel[Acid/Gas/Oil]Single00" blocks: Exploding upright barrel
15. "cntBarrel[Acid/Gas/Oil]Single45"/"cntBarrelAcidQuad[A/B]" blocks: Exploding angled barrel
16. "cntBarrel[Gas/Oil]Quad[A/B]" blocks: Exploding quad barrels
17. "cntBarrelGenericSingle00" block: Exploding upright generic barrel
18. "cntBarrelGenericSingle45" block: Exploding angled generic barrel
19. "cntBarrelGenericQuad[A/B]" blocks: Exploding quad generic barrels


