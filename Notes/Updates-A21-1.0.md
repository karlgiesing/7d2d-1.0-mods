# Updates from A21 to 1.0

## `dialogs.xml`
(relevant to Trader Lore)

* Addition of "jobshavenotallowed" response and statement
* Addition of `<requirement type="CheckCVar" id="IntroComplete" value="1" requirementtype="Hide" />`
    to all "jobshave" responses
* Removal of all `<action type="AddJournalEntry" id="questTip" />`
    from all "jobshave" responses
