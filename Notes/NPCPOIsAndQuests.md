# NPC POIs and Quests

This is a guide to get you started writing quests that target NPC POIs.

## Vanilla Quests and POIs

Since NPC quests are based on the vanilla quest system, let's go over how vanilla quests target POIs.

In order for a POI to be a target for vanilla quests, it must have a `QuestTags` property in its XML:
```xml
<property name="QuestTags" value="clear,fetch,restore_power" />
```
These tags tell the game that the POI will support a specific quest type.

Here are valid `QuestTags` values:
* `clear`: for clear quests, where a player must kill all entities that spawn in the POI's sleeper volumes.
* `fetch`: for fetch quests, where players are shown the quest satchel icon on their HUD.
* `hidden_cache`: like fetch quests, but players are _not_ shown the satchel icon.
  **This has been broken for several alphas.** You should not use it for new POIs.
* `restore_power`: restore power during the night. Introduced in A20.
* `trader`: used for quests that go to a trader, like "white river citizen" or "opening trade routes."
    _This tag must be used **only** on trader POIs, and trader POIs **must not** use any other quest tag._

Unfortunately, **quest tag values are hard-coded and cannot be changed.**
You cannot create a new kind of quest by adding a different quest tag.

Each quest type has its own POI requirements - for example, fetch quests require one or more quest loot helper blocks.
Since NPC quests have different considerations and quirks for each quest type, we'll go over them later.

Quests have different difficulty tiers, from tier 1 through tier 5, so your POI must also have a difficulty tier.
This is specified using the `DifficultyTier` tag in the POI XML:
```xml
<property name="DifficultyTier" value="4" />
```

In `quests.xml`, inside the quest itself, the POI is chosen using the vanilla `RandomPOIGoto` objective:
```xml
<objective type="RandomPOIGoto">
    <property name="phase" value="1"/>
    <property name="nav_object" value="quest" />
</objective>
```

Other quest objectives determine which `QuestTags` the POI must have:

* `ClearSleepers` requires `clear`
* `FetchFromContainer` requires `fetch`
* `POIBlockActivate` requires `restore_power`
* `Goto` requires `trader` (and that objective is used instead of `RandomPOIGoto`)

This is _additive,_ so a single quest can require more than one quest tag (like "fetch and clear" quests).

On the other hand, if your quest does not use any of these objectives, the POI does not need to have any `QuestTag` values.

The POI tier is specified by the `difficulty_tier` property in the quest:
```xml
<property name="difficulty_tier" value="1"/>
```

## Tagging NPC POIs

In the POI XML, tags are specified by the `Tags` property, which has been added for A20.
In vanilla, it is mainly used to specify districts or townships the POI may spawn into.

Here's an example, taken from `countrytown_business_01.xml`:
```xml
<property name="Tags" value="countrytown,rural,nodiagonal" />
```

Unlike the `QuestTags` values, **these values are not hard coded.**
A tag can be any string that does not have whitespace or commas.

This is what POI designers should use to specify what kind of NPC spawns into the POI.
In general, the tags should match the _sleeper volumes_ that are in the POI.

Here are the recommended tags to use for each kind of sleeper volume:

* `npc`: if the POI contains _any_ NPC Core sleeper volumes - so, all NPC POIs should have this tag
* `enemy`: if the POI contains any enemy NPC Core sleeper volumes:
    * Group NPC Bandits
    * Group NPC Whisperers
    * Group NPC Mech
    * Group NPC Fantasy
    * Group NPC Enemy Animals
    * Group NPC Arachnid
* `friendly`: if the POI contains any "friendly" (more accurately, "non-enemy") NPC Core sleeper volumes:
    * Group NPC Duke
    * Group NPC Military
    * Group NPC Vault
    * Group NPC Whiteriver
    * Group NPC Friendly Animals
* `bandit`: Group NPC Bandits
* `duke`: Group NPC Duke
* `military`: Group NPC Military
* `vault`: Group NPC Vault
* `whisperer`: Group NPC Whisperers
* `whiteriver`: Group NPC Whiteriver
* `mech`: Group NPC Mech
* `fantasy`: Group NPC Fantasy
* `enemyanimal`: Group NPC Enemy Animal
* `friendlyanimal`: Group NPC Friendly Animal
* `arachnid`: Group NPC Arachnid
* `flyer`: Group NPC Flyers
* `deco`: if this is a "deco" prefab with NPCs (used mainly in Age of Oblivion):
    * Group NPC Deco Trap
    * Group NPC Deco Rescue
    * Group NPC Deco Trader

These tags should be used _in addition to_ any vanilla tags.

For example, let's say you are creating a bandit POI that spawns into industrial areas.
Here is the XML you should use:
```xml
<property name="Tags" value="industrial,npc,enemy,bandit" />
```

For reference, here is the Google doc that acts as a specification:
https://docs.google.com/spreadsheets/d/1z23kOpd0cRYckDMV9b_oP3DlEzwpoJrDEp0wqnxBYI4/edit?usp=sharing

## Going to tagged POIs (or not)

The vanilla `RandomPOIGoto` quest objective does _not_ allow you to specify POI tags.
So, SCore includes a custom objective that does.

The `RandomTaggedPOIGotoSDX` quest objective allows you to _include_ or _exclude_ POIs with certain tags.
Here's an example that includes all "enemy" POIs, _except_ for Whisperers:

```xml
<objective type="RandomTaggedPOIGotoSDX, SCore">
    <property name="phase" value="1" />
    <property name="nav_object" value="quest" />
    <property name="include_tags" value="enemy" />
    <property name="exclude_tags" value="whisperer" />
</objective>
```

Both the `include_tags` and `exclude_tags` properties accept a comma-separated list of tags.
If a POI has _any_ of these tags, it will be included/excluded.

Here is how you could use a list to include all enemy POIs except Whisperers:
```xml
<objective type="RandomTaggedPOIGotoSDX, SCore">
    <property name="phase" value="1" />
    <property name="nav_object" value="quest" />
    <property name="include_tags" value="bandit,mech,fantasy,enemyanimal,arachnid" />
</objective>
```

This quest objective is a drop-in replacement for `RandomPOIGoto`, so it also accepts all the vanilla properties (like biome filters).

> _Note:_ Though we are using it to target NPC POIs, the new objective can be used to target any POI tag, including vanilla tags.
> For example, you could use it to send the player on a quest to clear a building _in a specific district._

### Preventing vanilla quests from going to NPC POIs

If you add `QuestTags` to your NPC POI, there is nothing preventing it from being targeted by vanilla quests.
This might be what you want, but in many cases it is not.

To avoid this, use the `RandomTaggedPOIGotoSDX` with _all vanilla_ quests, and exclude POIs with NPC tags.
Here is how you could do that using XPath:
```xml
<append xpath="//objective[@type='RandomPOIGoto']">
    <property name="exclude_tags" value="npc,bandit,duke,military,vault,whisperer,whiteriver" />
</append>
<set xpath="//objective[@type='RandomPOIGoto']/@type">RandomTaggedPOIGotoSDX, SCore</set>
```

## Special considerations for NPC quests

Because NPCs do not behave exactly like zombies,
there are things you must consider if you are using vanilla quest objectives with NPC POIs.

### Fetch

If you want your POI to support fetch quests, it must contain at least one (and ideally many) quest loot helper placeholder blocks.
When the quest is started, the game chooses one of these blocks to spawn the quest satchel;
the other blocks spawn loot instead (trash, ammo, book piles, etc).

The quest loot helper is named `cntQuestRandomLootHelper`, and it is called "= Quest Loot = Random Helper" in the prefab editor.

> _Note:_ There is a different helper block named `cntQuestNoPHRandomLootHelper`,
> which is called "= Quest Loot - No PH = Random Helper" in the prefab editor, and has a purple icon.
> If it is not spawning the quest satchel, it does not spawn placeholders (no loot).
> But TFP don't use it, and there are some issues with it, so it will likely get removed in the near future.
> **You should not use it.**

If you are not doing the quest on behalf of the Whiteriver faction,
it will be odd if players are fetching "Whiteriver Supplies" from the quest satchel.
It would be better if players were fetching faction-appropriate supplies (like "Military Supplies").

Fortunately, TFP already provide suplies for the Cassadores (the Duke's underlings).
They are called `questCassadoreSupplies`, and they are defined in `quest_items` in the `quests.xml` file.
We can use those supplies as a template to define supplies for other factions.

For example, if we want to create supplies for the military, we could add this to `quest_items`:
```xml
<quest_item id="3" name="questMilitarySupplies" description_key="questSuppliesGroupDesc" icon="questMilitarySupplies" icon_color="75,83,32"/>
```
Each additional kind of supplies should have a different `id` value.
(The icon color is the RGB value of "army green," but you could use any color.)

Next, add an entry to `Localization.txt` (I'm including only English for brevity):
```csv
Key,File,Type,english
questMilitarySupplies,items,Quest,Military Supplies
```

Finally, make sure the quest satchel spawns the right kind of supplies, by providing its `id` value.
You do this by specifying the `quest_item_ID` property in the `FetchFromContainer` objective:
```xml
<objective type="FetchFromContainer">
    <property name="quest_item_ID" value="3"/>
    <!-- ...all the other properties are the same as vanilla -->
</objective>
```

### Clear

To support clear quests, the POI must have one or more sleeper volumes that spawn entities.

In order for players to complete the quest, they must kill the spawned entities,
so the player must be able to harm those entities.
By default, as set in NPC Core, "neutral" human NPCs cannot be harmed.

NPCs that spawn into sleeper volumes can wander around quite a lot after they are awakened.
They can open unlocked doors, jump over fences, run after enemies outside the POI, etc.

This is problematic if the quest is to kill the NPCs that spawn into those sleeper volumes.
For one thing, NPCs can end up outside the POI, and the player can't complete the quest since they fail if they leave the area.
For another thing, the "tracker" that shows remaining enemies to clear, points to where they _spawned in,_ not to their current location.

So if your POI intends to support a clear quest:

* As much as possible, design the POI so NPCs can't wander far from where they're spawned in
    (using fences, locked doors, or other obstacles).
* Keep the sleeper volumes tight, so NPCs don't wake up until the player gets close to them.
* As a last resort, enlarge the radius of the `POIStayWithin` objective, or even eliminate it altogether.
    (Players will still need to hunt down "escaped" NPCs, but they won't automatically fail the quest.)

If you have more than one type of entity in the POI, like zombies and Whisperers,
you may want players to clear one type of entity (Whisperers) but not the other (zombies).

In the prefab editor, select the sleeper volume containing entities you _don't_ want to clear,
and set the "Quest Exclude" toggle to "Yes".

### Restore Power

NPC "restore power" quests are no different than vanilla, except that the entities spawned in the POI are NPCs.
The vanilla quests also require clearing the entities, but this is a separate objective which you can omit.

The main issue is whether it makes sense to do this kind of quest at all with NPC POIs.
POIs with non-enemy NPCs would make this trivial to complete.
But it's also far-fetched that enemy NPCs would try to kill players for fixing _their own power._

So if you are making NPC restore power quests, then you should add different context to make them believable or challenging.
Some ideas:

* For quests to non-enemy POIs, send the player on "fetch" quests to different _enemy_ POIs, under the pretext that they are gathering parts for the generators.
    Use `<reward type="Quest" id="[next quest ID]"/>` to chain them together, with the final quest being the "restore power" quest.
* For enemy quests, have the POI be empty until the player finishes the quest, then have the enemy NPCs "ambush" the player.
    For example, after completion, the player might have to exit the POI through a room that was previously locked by a switch,
    and that room contains enemy NPCs.

These are just suggestions.

#### Block activation events

In vanilla, when the generator is fixed, the lights in the POI go on.
But this does not have to be what happens.

In the `POIBlockActivate` quest objective, the completion action is set by this property:
```xml
<property name="complete_event" value="quest_poi_lights_on" />
```

The value can be the name of any `action_sequence` from the `gemeevents.xml` file.

Keep in mind that there can be only _one_ completion event.
So if you want the POI lights to go on, _and_ something else to happen, you will need to make your own action sequence.

## Custom quests and tags

In general, a POI only has sleeper volumes that spawn one kind (or faction) of NPC.
Quests will target the POI if it has the tag for that specific kind of NPC.

But this does not always have to be the case.
It is theoretically possible to have multiple kinds of NPCs in the same POI.
However, it is difficult to target _only_ POIs that have _both_ kinds of NPC.

Let's say you want to create a "Rescue" mission.
The quest sends the player to a POI with Whiteriver NPCs inside, and bandits outside attacking them.

This will **not** work:
```xml
<objective type="RandomTaggedPOIGotoSDX, SCore">
    <!-- ...snip... -->
    <property name="include_tags" value="bandit,whiteriver" />
</objective>
```

That will send players to POIs that have _either_ bandits _or_ Whiteriver NPCs.

The solution is to use a new POI tag in the quest objective:
```xml
<objective type="RandomTaggedPOIGotoSDX, SCore">
    <!-- ...snip... -->
    <property name="include_tags" value="banditvswhiteriver" />
</objective>
```
(The tag could be anything, I chose that because it's easy to understand.)

Your POI must also have that tag in its XML file:
```xml
<property name="Tags" value="industrial,npc,banditvswhiteriver" />
```

Since the player's job is to kill all the bandits, make it support "clear" quests:
```xml
<property name="QuestTags" value="clear" />
```

In the prefab editor, set up the POI sleeper volumes for both bandits and Whiteriver,
and set the Whiteriver sleeper volumes to "Quest Exclude."

Since both the quest and POI use a non-standard tag,
it is probably best if they are distributed together in a single mod or modlet.

Hope this helps!
