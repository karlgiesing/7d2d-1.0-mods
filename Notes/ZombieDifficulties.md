# Zombie Difficulties

## Easy:
zombieBoe
zombieFemaleFat
zombieJoe
zombieJanitor
zombieMoe
zombieArlene
zombieDarlene
zombieMarlene
zombieYo
zombieSteve
zombieSteveCrawler
zombieBusinessMan
zombieSpider
zombieNurse
zombieUtilityWorker
zombieSkateboarder
zombieCheerleader
zombieOldTimer
zombieBiker
zombieFarmer
zombieStripper

## Medium:
zombieWightFeral
zombieBoeFeral
zombieFootballPlayer
zombieFemaleFatFeral
zombieJoeFeral
zombieJanitorFeral
zombieMoeFeral
zombieArleneFeral
zombieScreamer
zombieDarleneFeral
zombieMarleneFeral
zombieYoFeral
zombieSteveFeral
zombieSteveCrawlerFeral
zombieBusinessManFeral
zombieSpiderFeral
zombieNurseFeral
zombieFatHawaiian
zombieFatCop
zombieUtilityWorkerFeral
zombieSkateboarderFeral
zombieCheerleaderFeral
zombieOldTimerFeral
zombieBikerFeral
zombieFarmerFeral
zombieStripperFeral

## Hard:
zombieWightRadiated
zombieBoeRadiated
zombieFemaleFatRadiated
zombieJoeRadiated
zombieJanitorRadiated
zombieMoeRadiated
zombieArleneRadiated
zombieScreamer
zombieDarleneRadiated
zombieMarleneRadiated
zombieYoRadiated
zombieSteveRadiated
zombieSpiderRadiated
zombieFatHawaiianFeral
zombieFatCopFeral
zombieFatCopRadiated
zombieSkateboarderRadiated
zombieCheerleaderRadiated
zombieOldTimerRadiated
zombieBikerRadiated
